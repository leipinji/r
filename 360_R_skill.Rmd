---
title: "360 R skill"
author: "David Lei"
date: "2016年8月31日"
output: html_document
---

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

### R 中message 函数的用法
message 函数可以设置调用包的时候不输出信息，也可以设置默认输出一些信息

1.**默认不输出信息**
```{r,warning=F}
library(gplots)
suppressMessages(library(gplots))
```

2.**默认输出一些信息**
```{r,warning=F}
info=function(){
  message('Start message for this function')
  Sys.sleep(2)
  message('End message for this function')
}
info()
```

### R 中 command line arguments 使用
1.**基础包 commandArgs 的用法**
```
#!/usr/bin/Rscript 
args=commandArgs(TRUE)
if (length(args)<1){
  args=c('--help')
}

## Help Section
if ('--help' %in% args){
cat ("
The R script usage:
  Options:
    --arg1=someValue  - numeric value
    --arg2=someValue  - character value
    --arg3=someValue  - logical value
    --help            - print help message

    Example:
    Rscript ./options.R --arg1=1 --arg2='test' --arg3=T \n\n")
q(save = 'no')  
}
parseArg=function(x){
  strsplit(sub("^--","",x),'=')
}
argsDF=as.data.frame(do.call('rbind',parseArg(args)))
options = as.list(as.character(argsDF$V2))
names(options)=argsDF$V1
if (is.null(options$arg1)){
  # do something
  cat("
  Please set --args1 value")
  q(save='no')
}
if (is.null(options$arg2)){
  # do something
  cat("
  Please set --args2 value")
  q(save='no')
}
if (is.null(options$arg3)){
  # do something
  cat("
  Please set --args3 value")
  q(save='no')
}
```

2. **getopt 包的使用**

```
#!/usr/bin/Rscript
library(getopt)
spec = matrix(c(
  'help','h','0','logical',
  'verbose','v','0','logical',
  'count','c','1','integer'
),ncol = 4,byrow = T)
opt = getopt(spec)
if (!is.null(opt$help)){
  cat(getopt(spec,usage=T))
  q(save='no')
}
if (opt$verbose){
  print("Verbose mode...")
}

print('Count value is ...')
print(opt$count)
```

3.**optparse 包的使用**

optparse 包默认会设置好 -h，--help 

```
#!/usr/bin/Rscript
library('optparse')
### 第一种用法
options = OptionParser()
options = add_option(options,
                     c('-v','--verbose'),
                     action='store_true',
                     help = 'Verbose mode...')
options = add_option(options,
                     c('-c','--count'),
                     action='store',
                     help = 'Test count value'
                     )
args=parse_args(options)
print(args$count)

### 第二种用法
options = list(
  make_option(c('-v','--verbose'),
              action='store_true',
              help='Verbose mode...'),
  make_option(c('-c','--count'),
              action='store',
              help='Test count value')
)

args=parse_args(OptionParser(option_list = options))
```

### R 中 正则表达式的用法

1.**sub 函数的用法**

```
for (file in dir()){
f.txt=sub("(.*).csv",'\\1.txt',file,perl=T)
print(f.txt)
}
```

}

### R 中 并集，交集，差集的操作

1. **sets包中union,intersect,setdiff的用法**

```{r}
a=letters[1:10]
b=letters[5:15]
a
b
union(a,b)
intersect(a,b)
### setdiff 非对称
setdiff(a,b)
setdiff(b,a)
```