---
title: "Heatmap"
author: "David Lei"
date: "2016年8月30日"
output: html_document
---

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.


```{r}
image(volcano,col=rainbow(200))
```