#!/usr/bin/Rscript 
args=commandArgs(TRUE)
if (length(args)<1){
  args=c('--help')
}

## Help Section
if ('--help' %in% args){
  cat ("
       The R script usage:
       Options:
       --arg1=someValue  - numeric value
       --arg2=someValue  - character value
       --arg3=someValue  - logical value
       --help            - print help message
       
       Example:
       Rscript ./options.R --arg1=1 --arg2='test' --arg3=T \n\n")
q(save = 'no')  
}
parseArg=function(x){
  strsplit(sub("^--","",x),'=')
}
argsDF=as.data.frame(do.call('rbind',parseArg(args)))
options = as.list(as.character(argsDF$V2))
names(options)=argsDF$V1
if (is.null(options$arg1)){
  # do something
  cat ("
Please set arg1 value")
  q(save = 'no')
}
if (is.null(options$arg2)){
  # do something
  cat ("
Please set arg2 value")
  q(save = 'no')
}
if (is.null(options$arg3)){
  # do something
  cat ("
Please set arg3 value")
  q(save = 'no')
}
print('Auguments ...')
print(unlist(args))
print('Options ...')
print(unlist(options))
